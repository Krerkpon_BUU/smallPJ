const express = require('express')
const router = express.Router()
const productsController = require('../controller/ProductsController')

/* GET products listing. */
router.get('/', productsController.getProducts)

router.get('/:id', productsController.getProduct)

router.post('/', productsController.addProduct)

router.put('/', productsController.updateProduct)

router.delete('/:id', productsController.deleteProduct)

module.exports = router
