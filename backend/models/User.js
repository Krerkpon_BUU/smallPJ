const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  login: String,
  password: String,
  status: Boolean
})

const User = mongoose.model('User', userSchema)

User.find(function (err, users) {
  if (err) return console.error(err)
  console.log(users)
})
module.exports = mongoose.model('User', userSchema)
