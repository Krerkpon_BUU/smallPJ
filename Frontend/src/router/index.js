import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/products',
    name: 'products',
    component: () => import('../views/Products')
  },
  {
    path: '/users',
    name: 'users',
    component: () => import('../views/Users')
  },
  {
    path: '/customers',
    name: 'customers',
    component: () => import('../views/Customers')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
